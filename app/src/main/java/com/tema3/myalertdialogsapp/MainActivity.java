package com.tema3.myalertdialogsapp;

import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    int posicion = -1;
    String[] array;
    private ArrayList<Integer> selectedItems;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_activity);

        Button button1 = findViewById(R.id.dialog1);
        button1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogNormal();
            }
        });
        Button button2 = findViewById(R.id.dialog2);
        button2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogListado();
            }
        });
        Button button3 = findViewById(R.id.dialog3);
        button3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogListadoCheckBox();
            }
        });

        Button button4 = findViewById(R.id.dialog4);
        button4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogListadoOpciones();
            }
        });

        Button button5 = findViewById(R.id.dialog5);
        button5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                customDialog();
            }
        });

        array = getResources().getStringArray(R.array.arrayColors);
    }

    @Override
    public void onBackPressed() {

        AlertDialog.Builder builder = new AlertDialog.Builder(this);

        builder.setTitle("Mensaje del sistema");
        builder.setMessage("¿Desea salir de la aplicación?");

        builder.setPositiveButton("Sí", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                finish();
            }
        });

        builder.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        AlertDialog dialog = builder.create();
        dialog.show();


    }

    public void dialogNormal() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);

        builder.setTitle("Mensaje del sistema");
        builder.setMessage("¿Desea salir de la aplicación?");

        builder.setPositiveButton("Sí", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                finish();
            }
        });

        builder.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        AlertDialog dialog = builder.create();
        dialog.show();

    }

    public void dialogListado() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Elige un color");
        builder.setItems(R.array.arrayColors, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Toast.makeText(MainActivity.this, "Item " + which, Toast.LENGTH_SHORT).show();
            }
        });
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    public void dialogListadoCheckBox() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Elige un color");
        builder.setSingleChoiceItems(R.array.arrayColors, -1, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Toast.makeText(MainActivity.this, "Item " + which, Toast.LENGTH_SHORT).show();
                //dialog.dismiss();
                posicion = which;
                String color = array[which];
                Toast.makeText(MainActivity.this, "Color: " + color, Toast.LENGTH_SHORT).show();
            }
        });
        builder.setNegativeButton("Cancelar", null);
        builder.setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Toast.makeText(MainActivity.this, "Item " + which, Toast.LENGTH_SHORT).show();
                dialog.dismiss();
            }
        });
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    public void dialogListadoOpciones() {
        selectedItems = new ArrayList<>();
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Elige un color");
        builder.setMultiChoiceItems(R.array.arrayColors, null, new DialogInterface.OnMultiChoiceClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which, boolean isChecked) {
                if (isChecked) {
                    selectedItems.add(which);
                } else {
                    if (selectedItems.contains(selectedItems.indexOf(which))) {
                        selectedItems.remove(selectedItems.indexOf(which));
                    }
                }
            }
        });

        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Toast.makeText(MainActivity.this, selectedItems.toString(), Toast.LENGTH_SHORT).show();
            }
        });

        AlertDialog dialog = builder.create();
        dialog.show();
    }

    public void customDialog() {
        SharedPreferences preferences = getSharedPreferences("prefs", MODE_PRIVATE);
        final SharedPreferences.Editor editor = preferences.edit();

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        View v = getLayoutInflater().inflate(R.layout.custom_dialog, null);
        final EditText user = v.findViewById(R.id.username);
        EditText pass = v.findViewById(R.id.password);
        builder.setView(v);

        String savedUser = preferences.getString("user", "");
        String savedPass = preferences.getString("pass", "");
        user.setText(savedUser);
        pass.setText(savedPass);

        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });

        builder.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Toast.makeText(MainActivity.this, "No se ha guardado.", Toast.LENGTH_SHORT).show();
            }
        });
        final AlertDialog dialog = builder.create();
        dialog.show();

        dialog.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String usuario = user.getText().toString();
                String password = pass.getText().toString();
                if (usuario.isEmpty() || password.isEmpty()) {
                    user.setError("Error");
                    pass.setError("Error");
                    Toast.makeText(MainActivity.this, "Uno de los campos está vacío", Toast.LENGTH_SHORT).show();
                } else {
                    editor.putString("user", usuario);
                    editor.putString("pass", password);
                    editor.apply();
                    dialog.dismiss();
                }
            }
        });
    }
}